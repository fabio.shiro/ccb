package br.com.evaltec.spi.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MessageBundle {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageBundle.class);
	private static final String DEAFULT_BUNDLE_NAME = "messages";
	private static final String DEFAULT_EXTENSION = ".properties";
	private static final String FAILED_TO_LOAD_PROPERTIES_FILE = "Falha ao ler arquivo de propriedades ''{0}''.";
	private static final String FAILED_TO_SET_MESSAGE_VALUE = "Falha ao definir o valor ''{1}'' para a mensagem ''{0}''.";
	private static final String MESSAGE_CLASS_HAS_NO_FIELDS = "Implementação ''{0}'' nao possui mensagens declaradas.";
	private static final String MESSAGE_SHOULD_BE_DECLARED = "Implementação ''{0}'' deve declarar uma mensagem chamada ''{1}''.";
	private static final String PROPERTY_IS_NULL_OR_EMPTY = "Arquivo de propriedade ''{0}'' atribui um valor nulo ou vazio para a propriedade ''{1}''.";
	private static final String UNDEFINED_FIELD = "Mensagem ''{0}'' nao foi definida.";
	private static final String THERE_ARE_WARNINGS = "{0} aviso(s) encontrados.";
	private static final String INSTRUCTIONS = "Verifique a classe ''{0}'' e o seguinte arquivo de propriedades ''{1}''.";

	protected static void initialize(String bundle, Class<?> clazz) {
		String bundleName = bundle.concat(".properties");
		Properties properties = loadPropertiesFile(bundleName, clazz);
		if (!properties.isEmpty()) {
			String clazzName = clazz.getCanonicalName();
			Field[] fields = clazz.getDeclaredFields();
			if (fields != null && fields.length != 0) {
				int messageValuesWarnings = setMessagesValues(bundleName, clazz, properties);
				int declaredFieldsWarnings = validateDeclaredFields(fields);
				int warnings = messageValuesWarnings + declaredFieldsWarnings;
				if (warnings > 0 && LOGGER.isWarnEnabled()) {
					LOGGER.warn(StringUtils.bind("{0} aviso(s) encontrados.", new Object[]{warnings}));
					LOGGER.warn(
							StringUtils.bind("Verifique a classe ''{0}'' e o seguinte arquivo de propriedades ''{1}''.",
									new Object[]{clazzName, bundleName}));
				}

			} else {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error(StringUtils.bind("Implementação ''{0}'' nao possui mensagens declaradas.",
							new Object[]{clazzName}));
				}

			}
		}
	}

	protected static void initialize(Class<?> clazz) {
		initialize("messages", clazz);
	}

	private static Properties loadPropertiesFile(String bundleName, Class<?> clazz) {
		Properties properties = new Properties();

		try {
			InputStream inputStream = clazz.getResourceAsStream("/" + bundleName);
			properties.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
			return properties;
		} catch (Exception var5) {
			String errorMessage = StringUtils.bind("Falha ao ler arquivo de propriedades ''{0}''.",
					new Object[]{bundleName});
			LOGGER.error(errorMessage);
			throw new IllegalArgumentException(errorMessage, var5);
		}
	}

	private static int setMessagesValues(String bundleName, Class<?> clazz, Properties properties) {
		int warnings = 0;
		Set<Entry<Object, Object>> entrySet = properties.entrySet();
		Iterator iterator = entrySet.iterator();

		while (iterator.hasNext()) {
			Entry<Object, Object> entry = (Entry) iterator.next();
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();

			try {
				Field field = clazz.getDeclaredField(key);
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}

				if (value != null && value.length() > 0) {
					field.set((Object) null, value);
				} else {
					if (LOGGER.isWarnEnabled()) {
						LOGGER.warn(StringUtils.bind(
								"Arquivo de propriedade ''{0}'' atribui um valor nulo ou vazio para a propriedade ''{1}''.",
								new Object[]{bundleName, key}));
					}

					++warnings;
				}
			} catch (NoSuchFieldException var10) {
				LOGGER.warn(StringUtils.bind("Implementação ''{0}'' deve declarar uma mensagem chamada ''{1}''.",
						new Object[]{clazz.getCanonicalName(), key}));
				++warnings;
			} catch (Exception var11) {
				LOGGER.warn(StringUtils.bind("Falha ao definir o valor ''{1}'' para a mensagem ''{0}''.",
						new Object[]{key, value}));
				++warnings;
			}
		}

		return warnings;
	}

	private static int validateDeclaredFields(Field[] fields) {
		int warnings = 0;
		Field[] var2 = fields;
		int var3 = fields.length;

		for (int var4 = 0; var4 < var3; ++var4) {
			Field field = var2[var4];

			try {
				if (field.get((Object) null) == null) {
					if (LOGGER.isWarnEnabled()) {
						LOGGER.warn(
								StringUtils.bind("Mensagem ''{0}'' nao foi definida.", new Object[]{field.getName()}));
					}

					++warnings;
				}
			} catch (Exception var7) {
				if (LOGGER.isWarnEnabled()) {
					LOGGER.warn("Falha ao ler arquivo de propriedades ''{0}''.", var7.getMessage());
				}
			}
		}

		return warnings;
	}
}